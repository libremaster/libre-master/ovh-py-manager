# -*- encoding: utf-8 -*-

import ovh
import argparse
import simplejson as json

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-c', dest="conf", help="config file", nargs=1)
parser.add_argument("credential_id", help="credential id", type=str)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# main
result = client.delete('/me/api/credential/' + args.credential_id)
print (json.dumps(result, indent=4))
