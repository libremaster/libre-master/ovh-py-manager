# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_instance(instance):

    if instance['monthlyBilling']:
        monthly_status = instance['monthlyBilling']['status']
    else:
        monthly_status = ''

    table.append([
        instance['id'],
        instance['name'],
        instance['flavorId'],
        instance['imageId'],
        instance['sshKeyId'],
        instance['created'],
        instance['region'],
        monthly_status,
        instance['status'],
        ])

# main
instances = client.get('/cloud/project/%s/instance' % PROJECT_ID)

# print
table = []
for instance in instances:
    get_instance(instance)

print (tabulate(table, headers=['ID', 'Name', 'flavor ID', 'image ID', 'sshKey ID',
                               'Creation date', 'Region', 'monthlyBilling', 'Status']))
