# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_user(user):

    role_a = []
    for role in user['roles']:
        role_a.append(role['name'])

    table.append([
        user['id'],
        user['username'],
        user['creationDate'],
        user['description'],
        user['status'],
        ','.join(role_a),
        ])

# main
users = client.get('/cloud/project/%s/user' % PROJECT_ID)

# print
table = []
for user in users:
    get_user(user)

print (tabulate(table, headers=['ID', 'Username', 'Creation date', 'Description', 'Status', 'Roles']))
