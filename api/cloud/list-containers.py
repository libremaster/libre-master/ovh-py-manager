# -*- encoding: utf-8 -*-

import os
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-u', dest="get_static_url", help="List static URL", action='store_true')
parser.add_argument('-c', dest="conf", help="config file", nargs='?')
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# def
def get_storage(storage_id):

    storage = client.get('/cloud/project/%s/storage/%s' % (PROJECT_ID,storage_id))

    ret = []
    ret.append(storage['name'])
    ret.append(storage['storedObjects'])
    ret.append(storage['storedBytes'])
    ret.append(storage['region'])
    ret.append(storage['public'])
    ret.append(storage['containerType'])
    if args.get_static_url:
        ret.append(storage['staticUrl'])
    ret.append(storage['archive'])

    table.append(ret)

# main
storages = client.get('/cloud/project/%s/storage' % PROJECT_ID)

# print
table = []
for storage in storages:
    get_storage(storage['id'])

headers = []
headers.append('Name')
headers.append('Stored Objects')
headers.append('Stored Bytes')
headers.append('Region')
headers.append('Public')
headers.append('Container Type')
if args.get_static_url:
    headers.append('Static URL')
headers.append('Archive')
print (tabulate(table, headers=headers))
