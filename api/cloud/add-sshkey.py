# -*- encoding: utf-8 -*-

import os
import ovh
import argparse

# parameters
parser = argparse.ArgumentParser()
parser.add_argument("key_name", help="Key name", type=str)
parser.add_argument("key_file", help="Key file", type=str)
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region", help="Region name", type=str)
parser.add_argument('-c', dest="conf", help="config file", nargs='?', required=True)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# data
body_data = {'name': args.key_name}

if args.region:
    body_data['region'] = args.region
#else:
#    body_data['region'] = ''

# lecture clé
with open(args.key_file, 'r') as keyfile:
    body_data['publicKey'] = keyfile.read().strip()

    client.post('/cloud/project/%s/sshkey' % PROJECT_ID, **body_data)
