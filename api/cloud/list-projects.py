# -*- encoding: utf-8 -*-

import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-c', dest="conf", help="config file", nargs=1)
args = parser.parse_args()

# create a client using configuration
client = ovh.Client(config_file=args.conf)

# def
def get_project(project_id):
    project = client.get('/cloud/project/%s' % str(project_id))

    table.append([
        project_id,
        project['description'],
        project['planCode'],
        project['unleash'],
        project['expiration'],
        project['creationDate'],
        project['orderId'],
        project['access'],
        project['status'],
        ])

# main
projects = client.get('/cloud/project')

# print
table = []
for project_id in projects:
    get_project(project_id)

print (tabulate(table, headers=['ID', 'Description', 'plan Code', 'unleash', 'Expiration',
                               'Creation date', 'Order ID', 'Access', 'Status']))
