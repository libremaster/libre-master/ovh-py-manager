# -*- encoding: utf-8 -*-

import os, sys
import ovh
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region_name", help="Region", type=str)
parser.add_argument('-a', dest="archive", help="Archive container?", action='store_true')
parser.add_argument('-c', dest="conf", help="config file", nargs=1, required=True)
parser.add_argument("storagename", help="Storage name", type=str)
args = parser.parse_args()

# OVH client
ovh_client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# region
if args.region_name:
    REGION_NAME = args.region_name
else:
    REGION_NAME = os.getenv('OS_REGION_NAME')

# check
if REGION_NAME == '' or not REGION_NAME:
    sys.exit('Region ?')

if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# def
def new_storage_with_user():
    # new storage
    body_data = {}
    body_data['archive'] = args.archive
    body_data['containerName'] = args.storagename
    body_data['region'] = REGION_NAME
    storage = ovh_client.post('/cloud/project/%s/storage' % PROJECT_ID, **body_data)

    # new user for this storage
    body_data = {}
    body_data['description'] = args.storagename
    body_data['right'] = 'all'
    user = ovh_client.post('/cloud/project/%s/storage/%s/user' % (PROJECT_ID, storage['id']), **body_data)

    # print
    print (tabulate([[storage['name'], user['username'], user['password']]], headers=['Storage name', 'Username', 'Password']))

# main
new_storage_with_user()
