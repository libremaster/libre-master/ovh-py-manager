# -*- encoding: utf-8 -*-

import os, sys
import ovh
import swiftclient
import argparse
from tabulate import tabulate

# parameters
parser = argparse.ArgumentParser()
parser.add_argument('-p', dest="project_id", help="Project ID", type=str)
parser.add_argument('-r', dest="region_name", help="Region", type=str)
parser.add_argument('-a', dest="archive", help="Archive container?", action='store_true')
parser.add_argument('-c', dest="conf", help="config file", nargs=1, required=True)
parser.add_argument("storagename", help="Storage name", type=str)
args = parser.parse_args()

# OVH client
ovh_client = ovh.Client(config_file=args.conf)

# project_id
if args.project_id:
    PROJECT_ID = args.project_id
else:
    PROJECT_ID = os.getenv('OS_TENANT_ID')

# region
if args.region_name:
    REGION_NAME = args.region_name
else:
    REGION_NAME = os.getenv('OS_REGION_NAME')

# check
if REGION_NAME == '' or not REGION_NAME:
    sys.exit('Region ?')

if PROJECT_ID == '' or not PROJECT_ID:
    sys.exit('Project ID ?')

# OpenStack connexion
swift_conn = swiftclient.Connection(
        authurl=os.getenv('OS_AUTH_URL'),
        user=os.getenv('OS_USERNAME'),
        key=os.getenv('OS_PASSWORD'),
        os_options={
            'user_domain_name': os.getenv('OS_USER_DOMAIN_NAME'),
            'project_domain_name': os.getenv('OS_PROJECT_DOMAIN_NAME'),
            'project_id': PROJECT_ID,
            'region_name': REGION_NAME},
        auth_version='3'
    )

# def
def new_storage_user():
    # new user
    body_data = {}
    body_data['description'] = args.storagename
    body_data['roles'] = []
    user = ovh_client.post('/cloud/project/%s/user' % PROJECT_ID, **body_data)

    # new container
    headers = {'X-Container-Read': PROJECT_ID + ':' + user['username'], 'X-Container-Write': PROJECT_ID + ':' + user['username']}
    if args.archive:
        headers['X-Storage-Policy'] = 'PCA'
    swift_conn.put_container(container=args.storagename, headers=headers)

    # print
    print (tabulate([[args.storagename, user['username'], user['password']]], headers=['Storage name', 'Username', 'Password']))

# main
new_storage_user()
